import 'package:flutter/material.dart';
void main() {
  runApp(regPage());
}

class regPage extends StatefulWidget {
  @override
  State<regPage> createState() => _regPageState();
}

class _regPageState extends State<regPage> {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        // appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(

            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  // height: 200,
                  child: Image.network(
                    "https://upload.wikimedia.org/wikipedia/th/thumb/2/27/Buu0001.jpeg/420px-Buu0001.jpeg",
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      dataStudent(),
                      timeTable(),
                      grade(),
                      calendar(),
                    ],
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      course(),
                      regInf(),
                      expenses(),
                      contact(),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.white,
                  height: 15,
                ),
                mainNews(),
                // Container(
                //   // height: 1000, width: 500,
                //   // margin: EdgeInsets.fromLTRB(20, 1, 20, 10),
                //   // decoration: BoxDecoration(
                //   //   borderRadius: BorderRadius.circular(0),
                //   //   color: Colors.greenAccent,
                //   // ),
                //   padding: EdgeInsets.all(10),
                //     child: Text(
                //       "ประกาศ สำคัญ !",
                //       style: TextStyle(
                //         fontSize: 18, color: Colors.red,
                //       ),
                //       textAlign: TextAlign.start,
                //     ),
                //   ),
                Divider(
                  color: Colors.white,
                  height: 5,
                ),
                news1(),
                news2(),
                Divider(
                  color: Colors.white,
                  height: 30,
                ),
                Container(
                  child: Text('BUU University' ,
                    style: TextStyle(fontWeight: FontWeight.bold),),
                ),
                Divider(
                  color: Colors.white,
                  height: 40,
                ),
              ],
            ),
          ),
        ],
      )
    ],
  );
}

Widget dataStudent() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.people,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ข้อมูลนิสิต",style: TextStyle(color: Colors.black,fontSize: 13)
      ),

    ],
  );
}

Widget timeTable() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.table_chart,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ตารางเรียน",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}

Widget grade() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.grade,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ผลการเรียน",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}
Widget calendar() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.calendar_month,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ปฏิทินการศึกษา",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}

Widget regInf() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.document_scanner,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ข้อมูลทะเบียน",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}

Widget course() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.book,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("หลักสูตร",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}

Widget expenses() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.money,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ค่าใช้จ่าย",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}

Widget contact() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.chat,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("ติดต่อ",style: TextStyle(color: Colors.black,fontSize: 13)
      ),
    ],
  );
}

Widget mainNews() {
  return Column(
    children: <Widget>[
      Container(
        height: 43, width: 450,
        decoration: BoxDecoration(
          color: Colors.red,
    ),
    child: Column(
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: Text('! ประกาศ สำคัญ !' ,
            style: TextStyle(fontSize: 17,color: Colors.white,fontWeight: FontWeight.bold),),
        ),
      ],
    ),
      )
  ],
  );
}

Widget news1() {
  return Column(
      children: <Widget>[
    Container(
      padding: EdgeInsets.all(10),
      child: Text(
        "1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565",
        style: TextStyle(
          fontSize: 14, color: Colors.red,
        ),
        textAlign: TextAlign.start,
      ),
    ),

    Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image(image: AssetImage("images/new1.png"),width: 350),
        ],
      ),
    ),
        // Divider(
        //   color: Colors.white,
        //   height: 10,
        // ),
  ],
  );
}

Widget news2() {
  return Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.all(20),
        child: Text(
          "2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย "
              "ปีการศึกษา 2565",
          style: TextStyle(
            fontSize: 14.5, color: Colors.red,
          ),
          textAlign: TextAlign.start,
        ),
      ),

      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(image: AssetImage("images/new2.jpg"),width: 350),
          ],
        ),
      ),
    ],
  );
}